/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.map_robotvsbomb;

/**
 *
 * @author Acer
 */
public class Robot {
    private int x;
    private int y;
    private char symbol;
    private TableMap map;
    
    public Robot(char symbol, int x, int y, TableMap map){
        this.symbol = symbol;
        this.x = x;
        this.y = y;
        this.map = map;
    }
    
    public char getSymbol() {
        return symbol;
    }
    
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean walk(char direction){
        switch(direction){
            case 'w':
            case 'N':
                if(walkN())return false;
                break;
            case 's':    
            case 'S':
                if(walkS())return false;
                break;
            case 'd':
            case 'E':
                if(walkE())return false;
                break;    
            case 'a':
            case 'W':
                if(walkW())return false;
                break;
            default:
                return false;
        }
        checkBomb();
        return true;
    }

    private void checkBomb() {
        if(map.isBomb(x, y)){
            foundBomb();
        }
    }

    private boolean walkW() {
        if (map.inMap(x-1, y)) {
            x-=1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkE() {
        if (map.inMap(x+1, y)) {
            x+=1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkS() {
        if (map.inMap(x, y+1)) {
            y+=1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkN() {
        if (map.inMap(x, y-1)) {
            y-=1;
        } else {
            return true;
        }
        return false;
    }
    
    private void foundBomb(){
        System.out.println("(" + x + ", " + y + ") Founded Bomb!!!");
    }
    
    public boolean isOn(int x, int y){
        return this.x == x && this.y == y;
    }
}
